#pragma once

#include <stdint.h>
#include <allegro5/allegro.h>

#ifdef AOI_USE_CONFIG_FILE
#include "../aoi_config.h"
#endif

#ifndef AOI_INTERNAL_WIDTH
#define AOI_INTERNAL_WIDTH 640
#endif
#ifndef AOI_INTERNAL_HEIGHT
#define AOI_INTERNAL_HEIGHT 360
#endif
#ifndef AOI_TILE_WIDTH
#define AOI_TILE_WIDTH 16
#endif
#ifndef AOI_TILE_HEIGHT
#define AOI_TILE_HEIGHT 16
#endif
#ifndef AOI_SPRITESHEET_PATH
#define AOI_SPRITESHEET_PATH "spritesheet.png"
#endif
#ifndef AOI_FPS
#define AOI_FPS 60.0
#endif

typedef ALLEGRO_COLOR aoi_color_t;

#define aoi_color(r,g,b) ((aoi_color_t){r,g,b,0.0})

int aoi_btn(uint8_t btn);
void aoi_cls(aoi_color_t color);
void aoi_spr(uint8_t spr, int x, int y, uint8_t flip);
void aoi_sprEX(uint8_t spr, int x, int y, uint8_t flip, float scale, float rot, int w, int h);
void aoi_line(int x0, int y0, int x1, int y1, aoi_color_t color);
void aoi_rect(int x, int y, int w, int h, uint8_t filled, aoi_color_t color);
void aoi_map(int x, int y);
